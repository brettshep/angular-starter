import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export enum CompName {
  home = 1,
}

const routes: Routes = [
  {
    path: 'home',
    data: { name: CompName.home },
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
